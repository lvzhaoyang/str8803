
#pragma once

#include <vector>
#include <string>
#include <cmath>

struct Map {

int min_x;
int max_x;
int min_y;
int max_y;

int size_x;
int size_y;

float offset_x;
float offset_y;
int resolution;

// pay attention to the coordinate of the map
// this is the different from opencv image coordinate
std::vector< std::vector<double> > prob;

/** Return true if it is an explored area
 * @param col
 * @param row
 * @return
 */
bool isExplored(int col,  int row) {
  return prob[col][row] > 0;
}

/** Return true if it is the free space
 * @param col
 * @param row
 * @return
 */
bool isFreeSpace(int col, int row) {
  return std::fabs(prob[col][row] - 0.0) < 1e-3;
}

};

struct Reading {
  double x_r;       ///< x coordinate of robot
  double y_r;       ///< y coordinate of robot
  double theta_r;   ///< heading of robot

  double x_l;       ///< x coordinate of laser
  double y_l;       ///< y coordinate of laser
  double theta_l;   ///< heading of laser

  std::vector<int> rs;    ///< laser reading

  double ts;        ///< timestamp

  /// check whether this is a laser reading
  bool isLaser() const { return (!rs.empty());}
};

typedef std::vector<Reading> Readings;

/** read the map for localization
 * @param mapName
 * @param map
 * @return
 */
int read_beesoft_map(const char* mapName, Map* map);

/** Read the data log
 * @param logName
 * @param reading
 * @return
 */
int read_log(const std::string logName, Readings& reading);

