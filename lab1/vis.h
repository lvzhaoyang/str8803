
#pragma once

#include "io.h"
#include <opencv2/core/core.hpp>

cv::Mat visualizeMap(const Map& map);

void visualizeReadings(const Readings& readings, const Map& map);
