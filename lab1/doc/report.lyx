#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing onehalf
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1in
\topmargin 1in
\rightmargin 1in
\bottommargin 1in
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Lab 1 Reprot
\end_layout

\begin_layout Author
Zhaoyang Lv
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

Yao Chen
\end_layout

\begin_layout Section
Summary
\end_layout

\begin_layout Standard
In this report, we briefly introduce the math and implementation algorithms.
 The entire lab project was written in C++.
 We are able to localize the robot position well qualitatively in all five
 provided datasets.
 The code is optimized with OpenMP.
 With OpenMP option on, we are able to localize 1000 particles in 0.25 second
 per frame.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/zlv30/develop/STR8803/lab1/results/robot1/1.png
	width 20page%

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
frame 1
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/zlv30/develop/STR8803/lab1/results/robot1/66.png
	width 20page%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
frame 66
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/zlv30/develop/STR8803/lab1/results/robot1/200.png
	width 20page%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
frame 200
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Float figure
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename /home/zlv30/develop/STR8803/lab1/results/robot1/500.png
	width 20page%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
frame 500
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Selected Images for the robotdata1.log (2000 particles)
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Model Description
\end_layout

\begin_layout Subsection
Particle filter
\end_layout

\begin_layout Standard
According to 
\begin_inset CommandInset citation
LatexCommand cite
key "key-1"

\end_inset

 (Section 4.3, Particle Filter), the particle filter can be modeled as 
\end_layout

\begin_layout Standard
\noindent
\begin_inset Formula 
\begin{equation}
bel(\mathbf{x}_{0:t})=\eta\cdot p(\mathbf{z}_{t}|\mathbf{x}_{t},\,\mathbf{m})p(\mathbf{x}_{t}|\mathbf{u}_{t},\,\mathbf{x}_{t-1})bel(\mathbf{x}_{0:t-1}),\label{eq:pf}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
\noindent
where 
\begin_inset Formula $p(\mathbf{x}_{t}|\mathbf{u}_{t},\,\mathbf{x}_{t-1})$
\end_inset

 represents the motion model, 
\begin_inset Formula $p(\mathbf{z}_{t}|\mathbf{x}_{t})$
\end_inset

 describes the sensor model, and 
\begin_inset Formula $\mathbf{m}$
\end_inset

 is the occupancy map.
\end_layout

\begin_layout Subsection
\noindent
Motion model
\end_layout

\begin_layout Standard
Let 
\begin_inset Formula $\mathbf{x}_{t}=\begin{bmatrix}x_{t} & y_{t} & \theta_{t}\end{bmatrix}^{T}$
\end_inset

 be the state vector representing the robot's pose at time 
\begin_inset Formula $t$
\end_inset

, where 
\begin_inset Formula $(x_{t},\,y_{t})$
\end_inset

 is the coordinate of the robot's center in the map (world) frame and 
\begin_inset Formula $\theta_{t}$
\end_inset

 is the heading direction in the same frame.
 The input vector is 
\begin_inset Formula $\mathbf{u}=\begin{bmatrix}\bar{\mathbf{x}}_{t-1} & \bar{\mathbf{x}}_{t}\end{bmatrix}^{T}$
\end_inset

, where 
\begin_inset Formula $\bar{\mathbf{x}}_{t}=\begin{bmatrix}\bar{x}_{t} & \bar{y}_{t} & \bar{\theta}_{t}\end{bmatrix}^{T}$
\end_inset

 is the robot's pose in odometry frame at time 
\begin_inset Formula $t$
\end_inset

.
 We used the algorithm in Table 5.6 of 
\begin_inset CommandInset citation
LatexCommand cite
key "key-1"

\end_inset

 to realize the sampling from 
\begin_inset Formula $p(\mathbf{x}_{t}|\mathbf{u}_{t},\,\mathbf{x}_{t-1})$
\end_inset

.
 The motion model is shown in Algorithm 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:sampling-motion-model"

\end_inset

.
 The function 
\begin_inset Formula $\text{\textbf{sample}}(\sigma^{2})$
\end_inset

 means taking a sample from a distribution with mean 0 and variance 
\begin_inset Formula $\sigma^{2}$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float algorithm
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $\delta_{\text{rot1}}=\text{atan2}(\bar{y}_{t}-\bar{y}_{t-1},\,\bar{x}_{t}-\bar{x}_{t-1})-\bar{\theta}_{t-1}$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $\delta_{\text{trans}}=\sqrt{(\bar{x}_{t}-\bar{x}_{t-1})^{2}+(\bar{y}_{t}-\bar{y}_{t-1})^{2}}$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $\delta_{\text{rot2}}=\bar{\theta}_{t}-\bar{\theta}_{t-1}-\delta_{\text{rot1}}$
\end_inset


\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $\hat{\delta}_{\text{rot1}}=\delta_{\text{rot1}}-\text{\textbf{sample}}(\alpha_{1}\delta_{\text{rot1}}^{2}+\alpha_{2}\delta_{\text{trans}}^{2})$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $\hat{\delta}_{\text{trans}}=\delta_{\text{trans}}-\text{\textbf{sample}}(\alpha_{3}\delta_{\text{trans}}^{2}+\alpha_{4}\delta_{\text{rot1}}^{2}+\alpha_{4}\delta_{\text{rot2}}^{2})$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $\hat{\delta}_{\text{rot2}}=\delta_{\text{rot2}}-\text{\textbf{sample}}(\alpha_{1}\delta_{\text{rot1}}^{2}+\alpha_{2}\delta_{\text{trans}}^{2})$
\end_inset


\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $x_{t}=x_{t-1}+\hat{\delta}_{\text{trans}}\cos(\theta_{t}+\hat{\delta}_{\text{rot1}})$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $y_{t}=y_{t-1}+\hat{\delta}_{\text{trans}}\sin(\theta_{t}+\hat{\delta}_{\text{rot1}})$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $\theta_{t}=\theta_{t-1}+\hat{\delta}_{\text{rot1}}+\hat{\delta}_{\text{rot2}}$
\end_inset

 
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

return 
\begin_inset Formula $\mathbf{x}_{t}=\begin{bmatrix}x_{t} & y_{t} & \theta_{t}\end{bmatrix}^{T}$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "alg:sampling-motion-model"

\end_inset

Sampling from motion model 
\begin_inset Formula $p(\mathbf{x}_{t}|\mathbf{u}_{t},\,\mathbf{x}_{t-1})$
\end_inset

.
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Sensor model
\end_layout

\begin_layout Standard
We used the beam model for the laser rangefinder.
 Let 
\begin_inset Formula $\mathbf{z}_{t}=\begin{bmatrix}z_{t}^{1} & z_{t}^{2} & \cdots & z_{t}^{180}\end{bmatrix}$
\end_inset

 be the measurement vector, where 
\begin_inset Formula $z_{t}^{k}$
\end_inset

 refers to the range measurement associated with the 
\begin_inset Formula $k$
\end_inset

-th beam.
 In theory, we assume that all 180 
\begin_inset Formula $z_{t}^{k}$
\end_inset

 are independent, given the particle current state parameters and the occupancy
 map.
 Hence, we have
\begin_inset Formula 
\begin{equation}
p(\mathbf{z}_{t}|\mathbf{x}_{t},\,\mathbf{m})=\prod_{k=1}^{180}p(z_{t}^{k}|\mathbf{x}_{t},\,\mathbf{m}).\label{eq:sm}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
Let 
\begin_inset Formula $z_{t}^{k*}$
\end_inset

 denote the true range of the obstacle measured by 
\begin_inset Formula $z_{t}^{k}$
\end_inset

.
 It is calculated from likelihood of the state vector 
\begin_inset Formula $\mathbf{x}_{t}$
\end_inset

 and the map 
\begin_inset Formula $\mathbf{m}$
\end_inset

 by ray casting.
 Rather than formulating the beam sensor model as a complicated multi-model
 distribution.
 We simply formulate each measurement model as a combination of Gaussian
 distribution and uniform distribution, 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
p(z_{t}^{k}|\mathbf{x}_{t},\,\mathbf{m})=\eta_{k}\exp\{-\frac{(z_{t}^{k}-z_{t}^{k*})^{2}}{2\sigma^{2}}\}+K,
\]

\end_inset


\end_layout

\begin_layout Standard
\noindent
wherein 
\begin_inset Formula $\eta_{m}$
\end_inset

 is a normalization factor and 
\begin_inset Formula $\sigma$
\end_inset

 is the standard deviation.
 The uniform distribution 
\begin_inset Formula $K$
\end_inset

 means that there is some constant probability that the sensor will return
 a garbage value, uniformly distributed across the range of the sensor.
 Hence, Equation 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:sm"

\end_inset

 becomes
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
p(\mathbf{z}_{t}|\mathbf{x}_{t},\,\mathbf{m})=\prod_{k=1}^{180}(\eta_{k}\exp\{-\frac{(z_{t}^{k}-z_{t}^{k*})^{2}}{2\sigma^{2}}\}+K).\label{eq:sensor-model}
\end{equation}

\end_inset


\end_layout

\begin_layout Subsection
Resampling
\end_layout

\begin_layout Standard
We followed the low variance resampling method in Table 4.4 of 
\begin_inset CommandInset citation
LatexCommand cite
key "key-1"

\end_inset

.
 The sampler is shown in Algorithm 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:resampling"

\end_inset

, wherein 
\begin_inset Formula $M$
\end_inset

 is the number of all particles and 
\begin_inset Formula $w_{t}^{[i]}$
\end_inset

 is the weight of the 
\begin_inset Formula $i$
\end_inset

-th particle 
\begin_inset Formula $\mathbf{x}_{t}^{[i]}$
\end_inset

.
 
\end_layout

\begin_layout Standard
\begin_inset Float algorithm
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $\bar{\mathcal{X}_{t}}=\textrm{Ø}$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $r=\text{rand}(0,\,\frac{1}{M})$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $c=w_{t}^{[1]}$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $i=1$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\series bold
for 
\begin_inset Formula $m=1$
\end_inset

 to 
\begin_inset Formula $M$
\end_inset

 do
\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $U=r+\frac{(m-1)}{M}$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\series bold
while 
\begin_inset Formula $U>c$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $i=i+1$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset Formula $c=c+w_{t}^{[i]}$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\series bold
endwhile
\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

add 
\begin_inset Formula $\mathbf{x}_{t}^{[i]}$
\end_inset

 to 
\begin_inset Formula $\bar{\mathcal{X}}_{t}$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\series bold
endfor
\end_layout

\begin_layout Plain Layout
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

return 
\begin_inset Formula $\bar{\mathcal{X}}_{t}$
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "alg:resampling"

\end_inset

Low variance sampler.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Implementation
\end_layout

\begin_layout Subsection
Particle initialization
\end_layout

\begin_layout Standard
For an initial particle 
\begin_inset Formula $\mathbf{x}_{t}^{[m]}=\begin{bmatrix}x_{t}^{[m]} & y_{t}^{[m]} & \theta_{t}^{[m]}\end{bmatrix}^{T}$
\end_inset

, the three entries are sampled from independent uniform distributions.
 If this candidate sits at the grid which has already been occupied by an
 obstacle, we discard it.
 Eventually, all initial particles are in the free spaces where the probability
 of occupancy is 
\begin_inset Formula $0$
\end_inset

.
 We set the weight of each particle to 
\begin_inset Note Comment
status open

\begin_layout Plain Layout
initial weight
\end_layout

\end_inset


\begin_inset Formula $ $
\end_inset

 .
\end_layout

\begin_layout Subsection
Motion model
\end_layout

\begin_layout Standard
There are four odometry specified parameters.
 They are set by 
\begin_inset Note Comment
status open

\begin_layout Plain Layout
alpha's
\end_layout

\end_inset


\begin_inset Formula $\alpha_{1}=1$
\end_inset

, 
\begin_inset Formula $\alpha_{2}=1$
\end_inset

, 
\begin_inset Formula $\alpha_{3}=1$
\end_inset

 and 
\begin_inset Formula $\alpha_{4}=1$
\end_inset

.
 If a particle moves out of the boundary of the map after motion, we let
 
\begin_inset Formula $w_{t}^{[m]}=0.001w_{t}^{[m]}$
\end_inset

.
 This particle will not survive after resampling.
 The value 
\begin_inset Formula $\alpha_{i}$
\end_inset

 determines the noise of particle in the motion model.
\end_layout

\begin_layout Subsection
Ray casting
\end_layout

\begin_layout Standard
Given the current pose of the robot 
\begin_inset Formula $\mathbf{x}_{t}^{[m]}$
\end_inset

, we shoot a ray with the angle 
\begin_inset Formula $\beta=\theta_{t}^{[m]}+\frac{i\cdot\pi}{180},\,i=-90,-89,\cdots,\,89$
\end_inset

.
 If this ray hits a grid with probability of occupancy greater than 0.5 in
 the map and the range 
\begin_inset Formula $z$
\end_inset

 is less than the maximum possible range of the laser sensor 
\begin_inset Formula $z_{\max}$
\end_inset

, we let the true range be 
\begin_inset Formula $z_{t}^{k*}=z$
\end_inset

.
 If the ray does not hit any obstacles, we let 
\begin_inset Formula $z_{t}^{k*}=z_{\max}$
\end_inset

.
\end_layout

\begin_layout Subsection
Sensor model
\end_layout

\begin_layout Standard
In order to eliminate numeric errors, Equation 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:sensor-model"

\end_inset

 can be rewritten approximated as
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align*}
\ln(p(\mathbf{z}_{t}|\mathbf{x}_{t},\,\mathbf{m})) & =\ln\bigg(\prod_{k=1}^{180}\eta_{k}\exp\{-\frac{(z_{t}^{k}-z_{t}^{k*})^{2}}{2\sigma^{2}}\}\bigg)+lnK\\
 & =\sum_{k=1}^{180}\ln\eta_{k}-\sum_{k=1}^{180}\frac{(z_{t}^{k}-z_{t}^{k*})^{2}}{2\sigma^{2}}+lnK.
\end{align*}

\end_inset


\end_layout

\begin_layout Standard
In implementation, we use a robust kernel function 
\begin_inset Formula $\rho(z_{t}^{k},z_{t}^{k*})$
\end_inset

 to truncate the large square error 
\begin_inset Formula $(z_{t}^{k}-z_{t}^{k*})^{2}$
\end_inset

.
 The variance was set to 
\begin_inset Formula $\sigma^{2}=5\cdot10^{5}$
\end_inset

.
 We also let 
\begin_inset Formula $C=\prod_{k=1}^{180}\eta_{k}=1000$
\end_inset

 and thus we have
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
p(\mathbf{z}_{t}|\mathbf{x}_{t},\,\mathbf{m})=C\cdot\exp\{-\sum_{k=1}^{180}\frac{\rho(z_{t}^{k},z_{t}^{k*})}{2\sigma^{2}}\}+10^{-3},
\]

\end_inset


\end_layout

\begin_layout Standard
\noindent
where 
\begin_inset Formula $10^{-3}$
\end_inset

 is the value of 
\begin_inset Formula $K$
\end_inset

.
\end_layout

\begin_layout Subsection
Resampling
\end_layout

\begin_layout Standard
We follow Algorithm 
\begin_inset CommandInset ref
LatexCommand ref
reference "alg:resampling"

\end_inset

 generally.
 In practice, resampling too often increase the risk of losing diversity.
 In our implementation, we only resample in the condition that the measurement
 model is updated, and meanwhile the robot is moving (not in a idle state).
 The weight of all particles are reset to 1, for numerical considerations.
 
\end_layout

\begin_layout Section
Results Discussion
\end_layout

\begin_layout Standard
The videos of particle filter can be visualied with the link 
\begin_inset Flex URL
status open

\begin_layout Plain Layout

https://drive.google.com/folderview?id=0Bzp6ftrYRB58V0hOaFc5T1hCVHc&usp=sharing
\end_layout

\end_inset

 
\end_layout

\begin_layout Standard
Besides this visualization, we also tried some sensitivity tests for our
 algorithm.
 Since there is not any ground truth or quantative metric to evaluate the
 algorithm.
 We will generally cover some intuitions we get in choosing the parameters.
\end_layout

\begin_layout Standard
The number of particles is an important factor to gaurantee the success.
 We found the results can converge generally with random initialization
 when the particle number is beyond 500.
 With proper parallization, we are affected by the program speed.
 With small number of particles, we need assure that noise model in motion
 update should be proper augmented.
 
\end_layout

\begin_layout Standard
The sensor model is critical in this implementation.
 With a uniform distribution 
\begin_inset Formula $K$
\end_inset

, the particle can survive from noises in the data.
 Since we use the likelihood of all beam measurements along the rays, the
 log error can be negatively very small which makes us suffer from a numerical
 issue.
 We set a very large variance for this likelihood to overcome this issue.
\end_layout

\begin_layout Standard
The motion model propagates the uncertainty of odometry.
 Since there is no system dynamics in this dataset, we set this value according
 to our expectation to the hebavior of particles.
 To prevent the particles fall into the trap of local optimal, this value
 is set to be relatively high.
 Finally all the particles will converge to a relatively big cluster, whose
 shape approxmiately represent the covariance.
\end_layout

\begin_layout Standard
\begin_inset Float table
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="5" columns="4">
<features rotate="0" tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<column alignment="center" valignment="top">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1000 particles
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
5000 particles
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
10000 particles
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
update motion model (ms)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0.5
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
4.12
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
update measurement model (ms)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
252.32
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
792.20
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1740.13
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
resampling (ms)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0.05
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0.06
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
0.06
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
TOTAL (ms)
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
253
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
794
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1745
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Implementation Performance (C++, parallelized by openMP).
 The major computation cost is the update of measurement model, which needs
 a raycasting of each particle in each angle direction.
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Future Work
\end_layout

\begin_layout Standard
In our implementation, we found the sensor model is quite imperfect.
 Large noises and non-gaussian errors weakens our Gaussian noise assumption
 greatly.
 One of the future work is to try a combination of all four possible ditribution
s in beam sensor as shown in 
\begin_inset CommandInset citation
LatexCommand cite
key "key-1"

\end_inset

.
 This sensor model should yield a better approximation to real measurement
 distribution.
 The parameters of the distributions need to be properly tuned.
 
\end_layout

\begin_layout Standard
In addition, some advanced resampling techniques can help improve the overall
 performance.
 Inappropriate resampling lead to a bad paricle diversity and local optimal.
 When the number of particles are small, we did not use any strategy to
 recover from that.
 Some random sampling can be done when the diversity is low.
 
\end_layout

\begin_layout Bibliography
\begin_inset CommandInset bibitem
LatexCommand bibitem
key "key-1"

\end_inset

Thrun, Sebastian, Wolfram Burgard, and Dieter Fox.
 Probabilistic robotics.
 MIT press, 2005.
\end_layout

\end_body
\end_document
