clear;
clc;
close all;

%% load traning data
fprintf('Loading data from the training dataset \n');
[trainingData, trainingLabels] = loadData(true);

trainingDataFilename = fullfile('..\data\TrainingData.mat');
save(trainingDataFilename, 'trainingData');

trainingLabelsFilename = fullfile('..\data\TrainingLabels.mat');
save(trainingLabelsFilename, 'trainingLabels');


%% load testing data
fprintf('Loading data from the testing dataset \n');
[testingData, testingLabels] = loadData(false);

testingDataFilename = fullfile('..\data\TestingData.mat');
save(testingDataFilename, 'testingData');

testingLabelsFilename = fullfile('..\data\TestingLabels.mat');
save(testingLabelsFilename, 'testingLabels');