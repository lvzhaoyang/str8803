function [data, label] = loadData(isTraining)
% loadTrainingData load training data from oakland_part3_am_rf.node_features
% data:     n-by-p matrix. Rows correspond to samples. Columns correspond to features
% label:    n-by-1 vector. Labels 

if isTraining
    fid = fopen('.\data\oakland_part3_am_rf.node_features');
    endLineIdx = 89825;
else
    fid = fopen('.\data\oakland_part3_an_rf.node_features');
    endLineIdx = 36400;
end

data = zeros(endLineIdx - 3, 10);
label = zeros(endLineIdx - 3, 1);

tline = fgets(fid);
lineIdx = 1;
while ischar(tline)    
    % get one line
    tline = fgets(fid);
    
    % update the index of the line
    lineIdx = lineIdx + 1;
    
    % get one line and load data
    if lineIdx > 3 && lineIdx <= endLineIdx
        fprintf('Loading data from Line %d...\n', lineIdx);
        if lineIdx == endLineIdx
            disp(tline);
        end
        lineData = str2num(tline);
        
        data(lineIdx - 3, :) = lineData(1, 6:15);
        label(lineIdx - 3, 1) = lineData(1, 5);
        fprintf('Done loading data from Line %d...\n', lineIdx);
    end    
end

fclose(fid);

end

