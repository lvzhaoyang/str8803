clear;
clc;
close all;

%% load two classes in both datasets
ds1Idx = 3;
ds2Idx = 5;
[trainX1, trainY1, testX1, testY1] = loadDataset1(ds1Idx, ds2Idx);
[trainX2, trainY2, testX2, testY2] = loadDataset2(ds1Idx, ds2Idx);

%% set up parameters
sigma = 0.1;    % std of the noise
n = size(trainX1, 2);
cov = eye(n);

%% calculate the estimated mean
useBatch = false;
if useBatch
    % dataset 1
    B1 = inv(cov) + trainX1' * trainX1 / sigma^2;
    Y1 = trainX1' * trainY1 / sigma^2;
    theta = inv(B1) * Y1;
    esitmatedY1 = testX1 * theta;
    error1 = sqrt(mean((esitmatedY1 - testY1).^2));
    
    % dataset 2
    B2 = inv(cov) + trainX2' * trainX2 / sigma^2;
    Y2 = trainX2' * trainY2 / sigma^2;
    theta = inv(B2) * Y2;
    esitmatedY2 = testX2 * theta;
    error2 = sqrt(mean((esitmatedY2 - testY2).^2));
else
    A = eye(n);
    B = 0;
    u = 0;
    P = cov;
    Q = zeros(n, n);
    R = sigma^2;
    theta = zeros(n, 1);
    
    for k = 1:size(trainX1, 1)
        C = trainX1(k, :);
        y = trainY1(k);
        [theta, P] = kf(A, B, C, Q, R, u, y, theta, P);
    end
    esitmatedY1 = testX1 * theta;
    error3 = sqrt(mean((esitmatedY1 - testY1).^2));
    
    P = cov;
    theta = zeros(n, 1);
    for k = 1:size(trainX2, 1)
        C = trainX2(k, :);
        y = trainY2(k);
        [theta, P] = kf(A, B, C, Q, R, u, y, theta, P);
    end
    esitmatedY2 = testX2 * theta;
    error4 = sqrt(mean((esitmatedY2 - testY2).^2));
end