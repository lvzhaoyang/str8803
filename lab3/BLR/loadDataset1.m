function [trainX, trainY, testX, testY] = loadDataset1(ds1Idx, ds2Idx)
%   loadData load data from the mat files

% dataset 1
temp1 = load('..\data\TrainingData.mat');
data1 = temp1.trainingData;

temp2 = load('..\data\TrainingLabels.mat');
labels1 = temp2.trainingLabels;

labels1(find(labels1 == 1004)) = 1;
labels1(find(labels1 == 1100)) = 2;
labels1(find(labels1 == 1103)) = 3;
labels1(find(labels1 == 1200)) = 4;
labels1(find(labels1 == 1400)) = 5;

mask11 = find(labels1 == ds1Idx);
Y11 = labels1(mask11);
X11 = data1(mask11, :);
mask12 = find(labels1 == ds2Idx);
Y12 = labels1(mask12);
X12 = data1(mask12, :);

X1 = [X11; X12];
Y1 = [Y11; Y12];
m = size(X1, 1);
p1 = randperm(m, floor(0.75 * m));
p2 = setdiff(1:m, p1);

trainX = X1(p1, :);
trainY = Y1(p1, 1);
testX = X1(p2, :);
testY = Y1(p2, 1);
end

