function [mu_x, P] = kf(A, B, C, Q, R, u, y, mu_x, P)
%kf Implement a one-step Kalman filter
%   dynamics:     x_dot = Ax + Bu + epsilon
%   observation:  y = Cx + xi
%   noise:        epsilon~N(0, Q), xi~N(0, R)

% prediction
mu_x = A * mu_x + B * u;    % apply dynamics
P = A * P * A' + Q;     % update P

% correction
K = P * C' * inv(C * P * C' + R);  % Kalman gain
i = y - C * mu_x;       % innovation
mu_x = mu_x + K * i;
P = (eye(size(A, 1)) - K * C) * P; 
end

