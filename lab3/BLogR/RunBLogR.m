clear;
clc;
close all;

%% load two classes in both datasets
ds1Idx = 3;
ds2Idx = 5;
[trainX1, trainY1, testX1, testY1] = loadDataset1(ds1Idx, ds2Idx);
[trainX2, trainY2, testX2, testY2] = loadDataset2(ds1Idx, ds2Idx);

%% set up parameters
sigma = 0.1;    % std of the noise
n = size(trainX1, 2);
cov = eye(n);

%% calculate the estimated mean
A = eye(n);
B = 0;
u = 0;
P = cov;
Q = zeros(n, n);
R = sigma^2;
theta = zeros(n, 1);

% dataset 1
for k = 1:size(trainX1, 1)
    C = trainX1(k, :);
    y = trainY1(k);
    [theta, P] = ekf(A, B, C, Q, R, u, y, theta, P);
end
esitmatedY1 = sigmoid(testX1 * theta);
error1 = sqrt(mean((esitmatedY1 - testY1).^2));

% dataset 2
P = cov;
theta = zeros(n, 1);
for k = 1:size(trainX2, 1)
    C = trainX2(k, :);
    y = trainY2(k);
    [theta, P] = ekf(A, B, C, Q, R, u, y, theta, P);
end
esitmatedY2 = sigmoid(testX2 * theta);
error2 = sqrt(mean((esitmatedY2 - testY2).^2));