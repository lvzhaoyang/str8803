function [f, g] = softmax(theta, X, y)
%
% Arguments:
%   theta - A vector containing the parameter values to optimize.
%       In minFunc, theta is reshaped to a long vector.  So we need to
%       resize it to an n-by-(num_classes-1) matrix.
%       Recall that we assume theta(:,num_classes) = 0.
%
%   X - The examples stored in a matrix.  
%       X(i, j) is the i'th coordinate of the j'th example.
%   y - The label for each example.  y(j) is the j'th example's label.
%

n = size(X, 1);

% append theta by a column vector of 0's
theta = reshape(theta, n, []);
num_classes = size(theta,2) + 1;
theta = [theta zeros(n, 1)];
 
% calculate h
hh = exp(theta' * X);
norm = sum(hh, 1);
h = bsxfun(@rdivide, hh, norm);

% calculate objective
mask = sub2ind(size(h), y, 1:size(h, 2));
temp1 = -log(h(mask));
f = sum(temp1);

% calculate gradient
temp2 = zeros(size(h));
temp2(mask) = 1;
g = -X * (temp2 - h)';
g = g(:, 1:num_classes - 1);
g = g(:);

end

