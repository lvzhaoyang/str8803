clear;
clc;
close all;

% add minfunc library
addpath(genpath('..\minFunc_2012\minFunc'));

% number of classes
nrClasses = 5;

uesBGD = false;

% load data
[trainingData, trainingLabels, testingData, testingLabels] = loadData();

% add row of 1s to the dataset to act as an intercept term.
trainingData = trainingData';
trainingData = [ones(1,size(trainingData, 2)); trainingData]; 

testingData = testingData';
testingData = [ones(1,size(testingData, 2)); testingData];

% get dimensions
m = size(trainingData, 2);  % number of samples
n = size(trainingData, 1);  % number of features per sample

% Train softmax classifier using minFunc
options = struct('MaxIter', 200);

% initialize the parameter vector
theta = rand(n, nrClasses - 1) * 0.001;

% check gradients
average_error = checkGradient(@softmax, theta(:), 40, trainingData, trainingLabels');

% apply gradient descent
if uesBGD
    theta(:) = minFunc(@softmax, theta(:), options, trainingData, trainingLabels');
    theta = [theta, zeros(n,1)]; % expand theta to include the last class.
else
    theta = theta(:);
    maxIters = 200;
    lastf = 1e6;
    error = 1;
    tol = 1e-4;
    iter = 1;
    eta = 0.0001;
    while error > tol && iter < maxIter
        p = randperm(m, m);
        for k = 1:m
            [f, g] = softmax(theta, trainingData(:, p(k)), trainingLabels(p(k)));
            theta = theta - eta * g;
        end
        error = abs(lastf - f) / lastf;
        lastf = f;
        fprintf('The error in iteration %d is %.5f\n', iter, error);
        iter = iter + 1;
    end
    theta = [theta; zeros(n, 1)];    
end

% Print out training accuracy.
accuracy = calculateAccuracy(theta, trainingData, trainingLabels');
fprintf('Training accuracy: %2.1f%%\n', 100 * accuracy);

% Print out test accuracy.
accuracy = calculateAccuracy(theta, testingData, testingLabels');
fprintf('Test accuracy: %2.1f%%\n', 100 * accuracy);
