function [cost, gradient, prediction] = ANN(theta, ei, data, labels, isPrediction)
%   ANNCost Calculate the cost, gradient and prediction.
%   This function uses the basic framework provided by UFLDL tutorial 
%   but the core functionality was implemented by Yao Chen.

% check whether the function is used for predicting the label
po = false;
if exist('isPrediction', 'var')
    po = isPrediction;
end;

% reshape into network
stack = params2stack(theta, ei);
numHidden = numel(ei.layer_sizes) - 1;  % number of hidden layers
hAct = cell(numHidden + 1, 1);          % a
gradStack = params2stack(theta, ei);    % stack of gradients

% forward propagation
act = data;
hAct(1, 1) = {act};
for k = 1:numHidden + 1
    z = bsxfun(@plus, stack{k}.W * act, stack{k}.b);    % calculate z 
    act = 1 ./ (1 + exp(-z));   % calculate action a
    if k < numHidden + 1
        hAct(k + 1, 1) = {act};         % load the actions  
    end
end

% return here if only predictions desired.
if po
    cost = -1; 
    gradient = [];  
  
    % calculate the prediction
    X = hAct{numHidden + 1, 1};
    W = stack{numHidden + 1}.W;
    hh = exp(W * X);
    norm = sum(hh, 1);
    prediction = bsxfun(@rdivide, hh, norm);
    return;
end;

% calculate the cost
X = hAct{numHidden + 1, 1};

W = stack{numHidden + 1}.W;
hh = exp(W * X);
norm = sum(hh, 1);
h = bsxfun(@rdivide, hh, norm);
y = labels;
mask = sub2ind(size(h), y', 1:size(h, 2));
temp1 = -log(h(mask));
cost = sum(temp1);

% calculate the gradient using back-propagation
temp2 = zeros(size(h));
temp2(mask) = 1;
delta = -(temp2 - h);

for k = numHidden + 1:-1:1
    aa = hAct{k, 1};
    gradStack{k}.W = delta * aa';
    gradStack{k}.b = sum(delta, 2);
    
    WW = stack{k}.W;
    ff = aa .* (1 - aa);
    delta = WW' * delta .* ff;
end

% reshape the gradient into a vector
gradient = stack2params(gradStack);
end



