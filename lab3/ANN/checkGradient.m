function averageError = checkGradient(fun, theta0, nrChecks, varargin)
%   checkGradient check whether the gradient is correct

delta = 1e-3; 
sumError = 0;

fprintf(' Iter       i             err');
fprintf('           g_est               g               f\n');

for i = 1:nrChecks
    T = theta0;
%     j = randsample(numel(T), 1);
    j = 4000 + i;
    T0 = T; 
    T0(j) = T0(j) - delta;
    T1 = T; 
    T1(j) = T1(j) + delta;

    [f, g] = fun(T, varargin{:});
    f0 = fun(T0, varargin{:});
    f1 = fun(T1, varargin{:});

    estimatedGradient = (f1 - f0) / (2 * delta);
    error = abs(g(j) - estimatedGradient);

    fprintf('% 5d  % 6d % 15g % 15f % 15f % 15f\n', ...
            i, j, error,g(j), estimatedGradient, f);

    sumError = sumError + error;
end

    averageError = sumError / nrChecks;
end
