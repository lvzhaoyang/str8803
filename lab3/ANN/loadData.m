function [trainingData, trainingLabels, testingData, testingLabels] = loadData()
%   loadData load data from the mat files

temp1 = load('..\data\TrainingData.mat');
trainingData = temp1.trainingData;

temp2 = load('..\data\TrainingLabels.mat');
trainingLabels = temp2.trainingLabels;
trainingLabels(find(trainingLabels == 1004)) = 1;
trainingLabels(find(trainingLabels == 1100)) = 2;
trainingLabels(find(trainingLabels == 1103)) = 3;
trainingLabels(find(trainingLabels == 1200)) = 4;
trainingLabels(find(trainingLabels == 1400)) = 5;

temp3 = load('..\data\TestingData.mat');
testingData = temp3.testingData;

temp4 = load('..\data\TestingLabels.mat');
testingLabels = temp4.testingLabels;
testingLabels(find(testingLabels == 1004)) = 1;
testingLabels(find(testingLabels == 1100)) = 2;
testingLabels(find(testingLabels == 1103)) = 3;
testingLabels(find(testingLabels == 1200)) = 4;
testingLabels(find(testingLabels == 1400)) = 5;

end

