clear;
clc;
close all;

%% setup environment
% a struct containing network layer sizes etc
ei = [];

% minfunc library
addpath(genpath('..\minFunc_2012\minFunc'));

%% load mnist data
[trainX1, trainY1, testX1, testY1] = loadDataset1();
[trainX2, trainY2, testX2, testY2] = loadDataset2();
trainingData = trainX2;
trainingLabels = trainY2;
testingData = testX2;
testingLabels = testY2;

%% populate ei with the network architecture to train
% ei is a structure you can use to store hyperparameters of the network
% the architecture specified below should produce  100% training accuracy
% You should be able to try different network architectures by changing ei
% only (no changes to the objective function code)

% dimension of input features
ei.input_dim = 10;
% number of output classes
ei.output_dim = 5;
% sizes of all hidden layers and the output layer
ei.layer_sizes = [256, ei.output_dim];
% scaling parameter for l2 weight regularization penalty
ei.lambda = 0;
% which type of activation function to use in hidden layers
% feel free to implement support for only the logistic sigmoid function
ei.activation_fun = 'logistic';

%% setup random initial weights
stack = initialize_weights(ei);
params = stack2params(stack);

%% setup minfunc options
options = [];
options.display = 'iter';
options.maxFunEvals = 1e6;
options.Method = 'lbfgs';

%% test gradient
% average_error = checkGradient(@ANN, params, 40, ei, trainingData', trainingLabels);

%% run training
useBGD = true;
if useBGD
    [opt_params,opt_value,exitflag,output] = minFunc(@ANN, ...
        params, options, ei, trainingData', trainingLabels);
else
    tempData = trainingData';
    m = size(tempData, 2);
    theta = params;
    maxIters = 200;
    lastCost = 1e6;
    error = 1;
    tol = 1e-4;
    iter = 1;
    eta = 0.5;
    while error > tol && iter < maxIters
        p = randperm(m, m);
        for k = 1:m
            [cost, gradient] = ANN(theta, ei, tempData(:, p(k)), trainingLabels(p(k)));
            theta = theta - eta * gradient;
            fprintf('\t The cost in iteration %d is %.5f\n', iter, cost);
        end
        error = abs(lastCost - cost) / lastCost;
        lastCost = cost;
        fprintf('The error in iteration %d is %.5f\n', iter, error);
        iter = iter + 1;
    end
    opt_params = theta;
end

%% compute accuracy on the test and train set
[~, ~, pred] = ANN( opt_params, ei, testingData', [], true);
[~, pred] = max(pred);
acc_test = mean(pred' == testingLabels);
fprintf('test accuracy: %f\n', acc_test);

[~, ~, pred] = ANN( opt_params, ei, trainingData', [], true);
[~, pred] = max(pred);
acc_train = mean(pred' == trainingLabels);
fprintf('train accuracy: %f\n', acc_train);
