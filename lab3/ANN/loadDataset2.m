function [trainX, trainY, testX, testY] = loadDataset2()
%   loadData load data from the mat files

% dataset 2
temp1 = load('..\data\TestingData.mat');
data1 = temp1.testingData;

temp2 = load('..\data\TestingLabels.mat');
labels1 = temp2.testingLabels;

labels1(find(labels1 == 1004)) = 1;
labels1(find(labels1 == 1100)) = 2;
labels1(find(labels1 == 1103)) = 3;
labels1(find(labels1 == 1200)) = 4;
labels1(find(labels1 == 1400)) = 5;

m = size(data1, 1);
p1 = randperm(m, floor(0.75 * m));
p2 = setdiff(1:m, p1);

trainX = data1(p1, :);
trainY = labels1(p1, 1);
testX = data1(p2, :);
testY = labels1(p2, 1);
end

